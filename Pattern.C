#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void print_first_last_row_data(unsigned int n)
{
    for (unsigned int first_last = 0; first_last < (n * 2 - 1); first_last++)
        printf("%d ", n);
    printf("\r\n");
}

int main(void)
{
    unsigned int n = 0;
    unsigned int print_cnt = 0;
    scanf_s("%d", &n);

    // Complete the code to print the pattern.
    /* Print first row and last row */
    print_first_last_row_data(n);
    print_cnt++;
    unsigned int repeat_element = 0;
    unsigned int incr_element = 0;
    for (unsigned int row_count = 1; row_count < (n*2) - 2; row_count++)
    {
        print_cnt = 0;
        unsigned int repeat_cnt = 0;
        unsigned int last_element = 0;

        for (unsigned int element_count = 0; element_count <= (n*2)-2; element_count++)
        {
            /* When n is 3, total rows to print are 5.
             * This condition is entered for first n rows.
             */
            if (row_count < n)
            {                
                if (element_count <= row_count)
                {
                    printf("%d ", n - element_count);
                    repeat_element = n - element_count;
                    print_cnt++;
                }
                else if (repeat_cnt < ((n * 2) - (print_cnt *2)))
                {
                   
                    printf("%d ", repeat_element);
                    repeat_cnt++;
                }
                else
                {
                    if (element_count == (n * 2) - 2)
                    {
                        print_cnt = 0;
                        repeat_cnt = 0;

                    }
                    printf("%d ", ++repeat_element);
                }
            }
            else
            {
                if (element_count < ((n*2) - 1 - row_count))
                {
                    printf("%d ", n - element_count);
                    repeat_element = n - element_count;
                    print_cnt++;
                }
                else if (repeat_cnt < ((n * 2) - (print_cnt * 2)))
                {
                    printf("%d ", repeat_element);
                    repeat_cnt++;
                }
                else
                {
                    printf("%d ", ++repeat_element);
                }
                //B                printf("* ");
            }
            //printf("\r\nElemCnt = %d Print_cnt = %d repeat_cnt=%d repeat_element =%d %d", 
                //element_count, print_cnt, repeat_cnt, repeat_element, (n * 2) - 1 - (print_cnt * 2));
        }
        printf("\r\n");
    }

    print_first_last_row_data(n);

    return 0;
}
